// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBLpgpYuPeiDopW7h5o8mcKHtUGAwgdT94",
    authDomain: "cf-pwa-a5fad.firebaseapp.com",
    databaseURL: "https://cf-pwa-a5fad.firebaseio.com",
    projectId: "cf-pwa-a5fad",
    storageBucket: "cf-pwa-a5fad.appspot.com",
    messagingSenderId: "513467316325",
    appId: "1:513467316325:web:1978db58e8f260b078ee8c",
    measurementId: "G-2GHWBPLLRB"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
