import { Component, OnInit } from '@angular/core';

import { ListService } from '../services/lists.service';

import { animations } from '../animations/animations';

@Component({
  selector: 'app-root',
  templateUrl: 'home.component.html',
  animations: [animations]
})
export class HomeComponent implements OnInit{
  public message: string;

  constructor(public listS : ListService){}

  ngOnInit(){
  }
}
