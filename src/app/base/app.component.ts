import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from 'angularfire2/auth';

import { PushNotificationsService } from '../services/push-notifications.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public token : any;
  public showPanel : boolean = false;

  constructor(public afAuth : AngularFireAuth, private router: Router, public pushS: PushNotificationsService ){}

  ngOnInit(){
    this.token = this.pushS.getSubscription();
    this.pushS.watchMessages();
    this.pushS.refreshToken();
  }

  setToken(){
    this.token = this.pushS.getSubscription();
    this.toggleNotificationsWindow();
  }

  requestPushPermission(){
    this.pushS.requestPermission().then(()=> this.setToken());
  }

  cancelPermission(){
    this.pushS.cancelPermission().then(()=> this.setToken());
  }

  toggleNotificationsWindow(){
    this.showPanel = !this.showPanel;
  }

  title = 'PWA';

  logout(){
    this.afAuth.auth.signOut().then(()=>{
      this.router.navigate(["/login"]);
    })
  }
}
