import { Component, OnInit, Input } from '@angular/core';

import { ITodo, TStatus } from '../../structures/todos';

import { TodoService } from '../../services/todos.service';

import { animations } from '../../animations/animations';

import * as moment from 'moment';

moment.locale('es');

@Component({
  selector: 'todo-card',
  templateUrl: 'todo.card.component.html',
  animations:[animations]
})
export class TodoCardComponent implements OnInit{
  @Input() todo : ITodo;
  @Input() listId : string;

  public press : string = 'up';

  public moment : any = moment;

  constructor(private todoS : TodoService){}

  ngOnInit(){}

  complete(){
    this.todo.status = TStatus.Completed;

    setTimeout(() => this.todoS.update(this.listId, this.todo),300);
  }

}