import { trigger, style, transition, animate, query, stagger, state} from '@angular/animations';

export const animations = [
    trigger('enterState',[
        transition("* => *",[
            query(':enter',[
                style({transform: 'translateX(-100%)',opacity:0}),
                stagger(50,[
                    animate(200,style({transform: 'translateX(0)', opacity:1}))
                ])
            ],{optional:true})
        ])
    ]),
    trigger('statusAnimation',[
        state('0,void',style({
            transform:'translateX(0)',
            opacity:1
        })),
        state('1',style({
            transform:'translateX(-100%)',
            opacity:0
        })),
        transition('0 <=> 1',[
            animate(200,style({
                transform:'translateX(0)',
                opacity:1
            })),
            animate(200)
        ])
    ]),
    trigger('pressAnimation',[
        state('up,void',style({
            transform: 'translateX(0)'
        })),
        state('down',style({
            transform: 'translateX(-100px)'
        })),
        transition('up <=> down',[
            animate(100,style({transform:'translateX(0)'})),
            animate(100)
        ])
    ])]