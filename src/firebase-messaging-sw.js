importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyBLpgpYuPeiDopW7h5o8mcKHtUGAwgdT94",
    authDomain: "cf-pwa-a5fad.firebaseapp.com",
    databaseURL: "https://cf-pwa-a5fad.firebaseio.com",
    projectId: "cf-pwa-a5fad",
    storageBucket: "cf-pwa-a5fad.appspot.com",
    messagingSenderId: "513467316325",
    appId: "1:513467316325:web:1978db58e8f260b078ee8c",
    measurementId: "G-2GHWBPLLRB"
})

const messaging = firebase.messaging();